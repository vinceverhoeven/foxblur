<?php
/**
 * Plugin Name: Webmango Projecten
 * Description: Adds projects post type
 * Version: 1.0
 * Author: Vince Verhoeven
 * Author URI: www.webmango.nl
 **/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

include_once ("model/WebmangoProjectenInit.php");
include_once ("model/WebmangoProjectenOverview.php");



