<?php
$image_id = get_post_meta($post->ID, 'projecten_thumbnail', true); ?>
<div class="editor-post-featured-image__container">
    <?php if ($image = wp_get_attachment_image_src($image_id)) :
        echo '<a href="#" class="misha-upl"><img src="' . $image[0] . '" /></a>
	      <a href="#" class="misha-rmv components-button is-secondary">Remove image</a>
	      <input type="hidden" name="projecten_thumbnail" value="' . $image_id . '">';
    else :
        echo '<a href="#" class="misha-upl">Upload image</a>
	      <a href="#" class="misha-rmv components-button is-secondary" style="display:none">Remove image</a>
	      <input type="hidden" class="input_projecten_thumbnail" name="projecten_thumbnail" value="">';
    endif; ?>
</div>
<script>
    jQuery(function ($) {

// on upload button click
        $('body').on('click', '.misha-upl', function (e) {

            e.preventDefault();

            var button = $(this),
                custom_uploader = wp.media({
                    title: 'Insert image',
                    library: {
                  //      uploadedTo: wp.media.view.settings.post.id, // attach to the current post?
                        type: 'image'
                    },
                    button: {
                        text: 'Use this image' // button label text
                    },
                    multiple: false
                }).on('select', function () { // it also has "open" and "close" events
                    var attachment = custom_uploader.state().get('selection').first().toJSON();
                    button.html('<img src="' + attachment.url + '">');
                    $('.input_projecten_thumbnail').val(attachment.id);
                }).open();

        });

// on remove button click
        $('body').on('click', '.misha-rmv', function (e) {

            e.preventDefault();

            var button = $(this);
            button.next().val(''); // emptying the hidden field
            button.hide().prev().html('Upload image');
        });

    });

</script>
