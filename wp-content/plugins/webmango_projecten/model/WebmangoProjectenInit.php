<?php

Class WebmangoProjectenInit
{
    const PLUGIN_NAME = 'projecten';

    const POST_NAME = 'Projecten';

    public function __construct()
    {
        add_action( 'init', array( $this, 'init' )  );
        add_filter( 'post_type_link', array( $this, 'category_post_link' ), 1, 3 );
        add_action('save_post', array($this, 'save_post'));
    }

    public function init()
    {
        $singular = __('Project');
        $plural = __(self::POST_NAME);

        $labels = array(
            'name' => $plural,
            'singular' => $singular,
            'add_name' => 'Voeg nieuwe',
            'add_new_item' => 'Nieuwe ' . $singular,
            'not_found' => 'Geen Projecten gevonden'
        );

        $args = array(
            'taxonomies'  => array( 'category' ),
            'labels' => $labels,
            'menu_position' => 5,
            'menu_icon'           => 'dashicons-admin-multisite',
            'public' => true,
            'rewrite' => array('slug' => 'project/%category%'),
            'supports' => array(
                'title',
                'thumbnail',
                'editor', // post content
            ),
            'show_in_rest' => true,
            'register_meta_box_cb' => array($this, 'addMetaBox'),
        );
        register_post_type(self::POST_NAME, $args);
    }

    public function category_post_link( $post_link, $id = 0 ){
        $post = get_post($id);
        if ( is_object( $post ) ){
            $terms = wp_get_object_terms( $post->ID, 'category' );
            if( $terms ){
                return str_replace( '%category%' , $terms[0]->slug , $post_link );
            }
        }
        return $post_link;
    }


    public function addMetaBox()
    {
        add_meta_box(
            "thumbnail",
            __('thumbnail'),
            array($this, 'admin_content'),
            self::PLUGIN_NAME,
            'side',
            'core'
        );
    }

    public function admin_content($post)
    {
        wp_nonce_field(basename(__FILE__), 'admin_nonce');
        $dwwp_stored_meta = get_post_meta($post->ID);
        include(plugin_dir_path(__FILE__) . '../view/templates/post_admin.php');
    }

    public function save_post($post_id)
    {

        if ( isset( $_POST['projecten_thumbnail'] ) ) {
            update_post_meta( $post_id, 'projecten_thumbnail', sanitize_text_field( $_POST['projecten_thumbnail'] ) );
        }
    }
}

$WebmangoProjectenInstance = new WebmangoProjectenInit();

?>