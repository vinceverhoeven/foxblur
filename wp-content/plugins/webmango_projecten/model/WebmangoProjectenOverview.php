<?php

class WebmangoProjectenOverview
{
    const GRID = array(
        "col-md-12",
        "col-md-6",
        "col-md-6",
        "col-md-4",
        "col-md-8",
        "col-md-8",
        "col-md-4",
        "col-md-6",
        "col-md-6",
        "col-md-8",
        "col-md-4",
        "col-md-6",
        "col-md-6",
        "col-md-12",
    );

    public function getCategory()
    {
        $cat = false;
        if (!is_single()) {
            $cate = get_category(get_query_var('cat'));
            if ($cate) {
                $cat = $cate->slug;
            }
        }
        return $cat;
    }

    public function getCategoryPosts($cat)
    {
        $args = array(
            'category_name' => $cat,
            'post_type' => WebmangoProjectenInit::POST_NAME,
            'post_status' => 'publish',
            'posts_per_page' => -1
        );
        return new WP_Query($args);
    }


    public function getAllPosts()
    {
        $args = array(
            'post_type' => WebmangoProjectenInit::POST_NAME,
            'post_status' => 'publish',
            'posts_per_page' => -1
        );
        return new WP_Query($args);
    }

    public function getGrid($count)
    {
        $gridValue = "col-md-6";
        if (isset(self::GRID[$count]))
            $gridValue = self::GRID[$count];

        return $gridValue;
    }

    public function getThumbnailOrFeatured($postId)
    {
        $image_id = get_post_meta($postId, 'projecten_thumbnail', true);
        if ($image = wp_get_attachment_image_src($image_id,'full')) {
            $url = $image[0];
        } else {
            $url = get_the_post_thumbnail_url($postId);
        }
        return $url;
    }
}

$WebmangoProjectenOverviewInstance = new WebmangoProjectenOverview();