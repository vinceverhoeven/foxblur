<section class="admin-custom-metabox ">
    <div class="row">
        <div class="col-md-12">
            <form>
                <div class="form-group">
                    <label class="font-md required">Snippet Id</label>
                    <input type="text" class="form-control input-max form-required" name="snippet_id" maxlength="30"
                           value="<?php if ( ! empty ( $dwwp_stored_meta['snippet_id'] ) ) {
                               echo esc_attr( $dwwp_stored_meta['snippet_id'][0] );
                           } ?>" required>
                </div>
                <div class="form-group">
                    <label class="font-md">Content</label>
                    <?php
                    $content  = get_post_meta( $post->ID, 'snippet_content', true );
                    $editor   = 'snippet_content';
                    $settings = array(
                        'textarea_rows' => 8,
                        'media_buttons' => true,
                    );
                    wp_editor( $content, $editor, $settings ); ?>
                </div>
            </form>
        </div>
    </div>
</section>
<?php
wp_enqueue_script( 'admin.js', plugins_url() .'/'. WebmangoSnippetsInit::PLUGIN_NAME . '/view/js/admin.js', array( 'jquery' ) );
add_action( 'wp_enqueue_scripts', 'webmango_snippet_scripts',1 );
?>