jQuery(document).ready(function ($) {

    $("input#publish").on('click',function (e) {
        var input =   $("input[name='snippet_id']");
        if(!validateText(input.val())){
            input.addClass('required');
            e.preventDefault();
            return false;
        }
    });

    function validateText(value){
        var validated = false;
        if(value.trim()){
            validated = true;
        }
        return validated;
    }

});