<?php

Class WebmangoSnippetsInit
{
    const PLUGIN_NAME = 'webmango_snippets';

    public function __construct()
    {
        add_action('init', array($this, 'init'));
        add_action('save_post', array($this, 'save_post'));
        add_filter('manage_' . self::PLUGIN_NAME . '_posts_columns', array($this, 'set_columns'));
        add_action('manage_' . self::PLUGIN_NAME . '_posts_custom_column', array($this, 'set_custom_column'), 10, 2);
        add_filter('manage_posts_columns', array($this, 'order_columns_head'));
    }

    public function init()
    {
        $singular = __('Snippet');
        $plural = __('Webmango Snippets');

        $labels = array(
            'name' => $plural,
            'singular' => $singular,
            'add_name' => 'Voeg nieuwe',
            'add_new_item' => 'Nieuwe ' . $singular,
            'not_found' => 'Geen Projecten gevonden'
        );

        $args = array(
            'labels' => $labels,
            'menu_position' => 5,
            'menu_icon' => 'dashicons-buddicons-replies',
            'public' => true,
            'publicly_queryable' => false,
            'has_archive' => false,
            'register_meta_box_cb' => array($this, 'addMetaBoxSnippetID'),
            'supports' => array(
                'title',
                'thumbnail',
            ),
        );
        register_post_type(self::PLUGIN_NAME, $args);
    }

    public function addMetaBoxSnippetID()
    {
        add_meta_box(
            'snippet',
            __('Snippet'),
            array($this, 'snippet_admin_content'),
            self::PLUGIN_NAME,
            'normal',
            'core'
        );
    }

    public function snippet_admin_content($post)
    {
        wp_enqueue_style('admin.css', plugins_url() . '/' . self::PLUGIN_NAME . '/view/css/admin.css');
        wp_nonce_field(basename(__FILE__), 'snippet_admin_nonce');
        $dwwp_stored_meta = get_post_meta($post->ID);
        include(plugin_dir_path(__FILE__) . '../view/templates/snippet_admin.php');
    }

    public function save_post($post_id)
    {
        $is_autosave = wp_is_post_autosave($post_id);
        $is_revision = wp_is_post_revision($post_id);
        $is_valid_nonce = (isset($_POST['dwwp_jobs_nonce']) && wp_verify_nonce($_POST['snippet_admin_nonce'], basename(__FILE__))) ? 'true' : 'false';

        // Exits script depending on save status
        if ($is_autosave || $is_revision || !$is_valid_nonce) {
            echo 'not valid';
            return;
        }

        if (isset($_POST['snippet_id'])) {
            update_post_meta($post_id, 'snippet_id', sanitize_text_field($_POST['snippet_id']));
        }

        if (isset($_POST['snippet_content'])) {
            update_post_meta($post_id, 'snippet_content', preg_replace("/\r\n|\r|\n/", '<br/>', $_POST['snippet_content']));
        }
    }

    public function set_columns($columns)
    {
        if (get_current_screen()->post_type !== self::PLUGIN_NAME)
            return $columns;

        $columns['snippet_id'] = __('Snippet id');
        return $columns;
    }


    function set_custom_column($column, $post_id)
    {
        if (get_current_screen()->post_type !== self::PLUGIN_NAME)
            return;


        switch ($column) {
            case 'snippet_id' :
                echo get_post_meta($post_id, 'snippet_id', true);
                break;
        }
    }

    function order_columns_head($defaults)
    {
        if (get_current_screen()->post_type !== self::PLUGIN_NAME)
            return $defaults;

        $new = array();
        $tags = $defaults['snippet_id'];
        unset($defaults['snippet_id']);

        foreach ($defaults as $key => $value) {
            if ($key == 'date') {
                $new['snippet_id'] = $tags;
            }
            $new[$key] = $value;
        }

        return $new;
    }
}

$WebmangoSnippetsInitInstance = new WebmangoSnippetsInit();

?>