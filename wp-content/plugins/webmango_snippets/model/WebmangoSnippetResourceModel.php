<?php
/**
* Date 22-04-20
* Author Vince Verhoeven
*/

class WebmangoSnippetResourceModel extends WebmangoSnippetsInit
{
    const WARN_TEXT = "snippet not exist";

    public function getSnippet($snippet_id)
    {
        $snippet = new WP_Query(array(
            'post_type' => self::PLUGIN_NAME,
            'post_status' => 'publish',
            'posts_per_page' => 1,
            'meta_query' => array(
                array(
                    'key' => 'snippet_id',
                    'value' => $snippet_id,
                    'compare' => '=',
                )
            )
        ));

        return $this->mapObject($snippet->posts);
    }

    protected function mapObject($posts)
    {
        $snippetArray = array();
        $posts = array_map(
            function ($post) {
                return array(
                    'post_id' => $post->ID,
                    'snippet_title' => $post->post_title,
                    'snippet_content' => get_post_meta($post->ID, 'snippet_content', true),
                    'snippet_image' => get_the_post_thumbnail_url($post->ID)
                );
            },
            $posts
        );
        if (isset($posts[0]))
            $snippetArray = $posts[0];
        return $snippetArray;
    }

    public function getId($snippetArray)
    {
        $id = self::WARN_TEXT;
        if (isset($snippetArray['post_id'])) {
            $id = $snippetArray['post_id'];
        }
        return $id;
    }

    public function getTitle($snippetArray)
    {
        $title = self::WARN_TEXT;
        if (isset($snippetArray['snippet_title'])) {
            $title = $snippetArray['snippet_title'];
        }
        return $title;
    }

    public function getContent($snippetArray)
    {
        $content = self::WARN_TEXT;
        if (isset($snippetArray['snippet_content'])) {
            $content = $snippetArray['snippet_content'];
        }
        return $content;
    }

    public function getImage($snippetArray)
    {
        $image = self::WARN_TEXT;
        if (isset($snippetArray['snippet_image'])) {
            $image = $snippetArray['snippet_image'];
        }
        return $image;
    }

}