<?php
/**
 * Plugin Name: Webmango Snippets
 * Description: Adds snippet post type
 * Version: 1.0
 * Author: Vince Verhoeven
 * Author URI: www.webmango.nl
 **/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

include_once ("model/WebmangoSnippetsInit.php");
include_once ("model/WebmangoSnippetResourceModel.php");



