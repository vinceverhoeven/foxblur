<?php
/**
 * Plugin Name: Webmango Medewerkers
 * Description: Adds medewerkers post type
 * Version: 1.0
 * Author: Vince Verhoeven
 * Author URI: www.webmango.nl
 **/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

include_once ("model/WebmangoMedewerkersInit.php");
include_once ("model/WebmangoMedewerkersOverview.php");



