<?php

Class WebmangoMedewerkersInit
{
    const PLUGIN_NAME = 'webmango_medewerkers';

    public function __construct()
    {
        add_action('init', array($this, 'init'));
        add_action('save_post', array($this, 'save_post'));
    }

    public function init()
    {
        $singular = __('Medewerker');
        $plural = __('Medewerker');

        $labels = array(
            'name' => $plural,
            'singular' => $singular,
            'add_name' => 'Voeg nieuwe',
            'add_new_item' => 'Nieuwe ' . $singular,
            'not_found' => 'Geen ' . $singular . ' gevonden'
        );

        $args = array(
            'labels' => $labels,
            'menu_position' => 5,
            'menu_icon' => 'dashicons-businessman',
            'public' => true,
            'has_archive' => false,
            'publicly_queryable'  => false,
            'rewrite' => array('slug' => strtolower($plural)),
            'register_meta_box_cb' => array($this, 'addMetaBox'),
            'supports' => array(
                'title',
                'thumbnail',
            ),
        );
        register_post_type(self::PLUGIN_NAME, $args);
    }

    public function addMetaBox()
    {
        add_meta_box(
            "medewerkers",
            __('Medewerker'),
            array($this, 'admin_content'),
            self::PLUGIN_NAME,
            'normal',
            'core'
        );
    }

    public function admin_content($post)
    {
        wp_enqueue_style('admin.css', plugins_url() . '/' . self::PLUGIN_NAME . '/view/css/admin.css');
        wp_nonce_field(basename(__FILE__), 'admin_nonce');
        $dwwp_stored_meta = get_post_meta($post->ID);
        include(plugin_dir_path(__FILE__) . '../view/templates/post_admin.php');
    }

    public function save_post($post_id)
    {
        $is_autosave = wp_is_post_autosave($post_id);
        $is_revision = wp_is_post_revision($post_id);
        $is_valid_nonce = (isset($_POST['dwwp_jobs_nonce']) && wp_verify_nonce($_POST['admin_nonce'], basename(__FILE__))) ? 'true' : 'false';

        // Exits script depending on save status
        if ($is_autosave || $is_revision || !$is_valid_nonce) {
            echo 'not valid';
            return;
        }

        if (isset($_POST['medewerker_functie'])) {
            update_post_meta($post_id, 'medewerker_functie', sanitize_text_field($_POST['medewerker_functie']));
        }

        if (isset($_POST['medewerker_email'])) {
            update_post_meta($post_id, 'medewerker_email', sanitize_text_field($_POST['medewerker_email']));
        }

        if (isset($_POST['medewerker_content'])) {
            update_post_meta($post_id, 'medewerker_content', preg_replace("/\r\n|\r|\n/", '<br/>', $_POST['medewerker_content']));
        }
    }
}

$WebmangoMedewerkersInstance = new WebmangoMedewerkersInit();

?>