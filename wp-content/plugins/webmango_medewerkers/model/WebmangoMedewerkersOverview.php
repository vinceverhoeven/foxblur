<?php

Class WebmangoMedewerkersOverview
{
    public function __construct()
    {
    }

    public function getAllPosts()
    {
        $args = array(
            'post_type' => WebmangoMedewerkersInit::PLUGIN_NAME,
            'post_status' => 'publish',
        );
        return new WP_Query($args);
    }
}

$WebmangoMedewerkersOverviewInstance = new WebmangoMedewerkersOverview();
?>