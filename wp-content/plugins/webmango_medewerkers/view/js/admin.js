jQuery(document).ready(function ($) {

    $("input#publish").on('click', function (e) {
        var good = true;

        var input = $("input[name='medewerker_functie']");
        var input2 = $("input[name='medewerker_email']");

        if (!validateText(input.val())) {
            input.addClass('required');
            good = false;
        }

        if (!validateText(input2.val())) {
            input2.addClass('required');
            good = false;
        }

        if (!good) {
            e.preventDefault();
            return false;
        }
    });

    function validateText(value) {
        var validated = false;
        if (value.trim()) {
            validated = true;
        }
        return validated;
    }

});