<section class="admin-custom-metabox ">
    <div class="row">
        <div class="col-md-12">
            <form>
                <div class="form-group">
                    <label class="font-md required">Functie</label>
                    <input type="text" class="form-control input-max form-required" name="medewerker_functie" maxlength="30"
                           value="<?php if ( ! empty ( $dwwp_stored_meta['medewerker_functie'] ) ) {
                               echo esc_attr( $dwwp_stored_meta['medewerker_functie'][0] );
                           } ?>" required>
                </div>
                <div class="form-group">
                    <label class="font-md required">Email</label>
                    <input type="text" class="form-control input-max form-required" name="medewerker_email" maxlength="30"
                           value="<?php if ( ! empty ( $dwwp_stored_meta['medewerker_email'] ) ) {
                               echo esc_attr( $dwwp_stored_meta['medewerker_email'][0] );
                           } ?>" required>
                </div>
                <div class="form-group">
                    <?php
                    $content  = get_post_meta( $post->ID, 'medewerker_content', true );
                    $editor   = 'medewerker_content';
                    $settings = array(
                        'textarea_rows' => 8,
                        'media_buttons' => false,
                    );
                    wp_editor( $content, $editor, $settings ); ?>
                </div>
            </form>
        </div>
    </div>
</section>

<?php
wp_enqueue_script( 'admin.js', plugins_url() .'/'. WebmangoMedewerkersInit::PLUGIN_NAME . '/view/js/admin.js', array( 'jquery' ) );
add_action( 'wp_enqueue_scripts', 'webmango_medewerkers_scripts',1 );
?>