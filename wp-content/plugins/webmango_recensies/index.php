<?php
/**
 * Plugin Name: Webmango Recensies
 * Description: Adds recensies post type
 * Version: 1.0
 * Author: Vince Verhoeven
 * Author URI: www.webmango.nl
 **/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

include_once ("model/WebmangoRecensiesInit.php");
include_once ("model/WebmangoRecensiesOverview.php");



