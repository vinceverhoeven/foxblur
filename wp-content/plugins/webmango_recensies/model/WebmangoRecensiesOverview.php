<?php

Class WebmangoRecensiesOverview
{
    public function __construct()
    {
    }

    public function getAllPosts()
    {
        $args = array(
            'post_type' => WebmangoRecensiesInit::PLUGIN_NAME,
            'post_status' => 'publish',
        );
        return new WP_Query($args);
    }
}

$WebmangoRecensiesOverviewInstance = new WebmangoRecensiesOverview();
?>