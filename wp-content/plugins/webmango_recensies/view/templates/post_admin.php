<section class="admin-custom-metabox ">
    <div class="row">
        <div class="col-md-12">
            <form>
                <div class="form-group">
                    <?php
                    $content  = get_post_meta( $post->ID, 'recensie_content', true );
                    $editor   = 'recensie_content';
                    $settings = array(
                        'textarea_rows' => 8,
                        'media_buttons' => false,
                    );
                    wp_editor( $content, $editor, $settings ); ?>
                </div>
            </form>
        </div>
    </div>
</section>

<?php
wp_enqueue_script( 'admin.js', plugins_url() .'/'. WebmangoRecensiesInit::PLUGIN_NAME . '/view/js/admin.js', array( 'jquery' ) );
add_action( 'wp_enqueue_scripts', 'webmango_recensies_scripts',1 );
?>