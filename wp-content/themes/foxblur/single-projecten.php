<?php
get_header();
?>
<div class="content-wrapper">
    <img class="full-image" src="<?php echo get_the_post_thumbnail_url(); ?>"/>
    <div class="page-projecten-detail">
        <div class="container">
            <div class="row">
                <div class="col-md-12 projecten-detail default-content">
                    <h1 class="default-content-title">
                        <?php echo get_the_title(); ?>
                    </h1>
                    <div class="default-content-content">
                        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <?php echo get_the_content(); ?>
                        <?php endwhile; endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();
