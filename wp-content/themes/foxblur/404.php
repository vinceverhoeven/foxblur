<?php
get_header(); ?>
    <div class="cms-page">
        <div class="page-wrapper">
            <div class="container">
                <h1 class="main-title">Niet gevonden</h1>
                <div class="row">
                    <div class="col-md-12 text-large-block">
                        <p>Deze pagina is niet gevonden</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer();
