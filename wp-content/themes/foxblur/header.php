<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?> >
<nav class="navbar navbar-expand-md fixed-top navbar-light bg-white">
    <div class="navbar-content">
        <?php the_custom_logo(); ?>
        <button class="navbar-toggler navbar-toggler-right collapsed" type="button" data-toggle="collapse"
                data-target="#collapsingNavbar">
            <span> </span>
            <span> </span>
            <span> </span>
        </button>
        <div class="collapse navbar-collapse justify-content-end" id="navbarCollapse">
            <?php
            wp_nav_menu(array(
                    'menu' => 'main_menu',
                    'theme_location' => 'primary',
                    'depth' => 2,
                    'container' => 'div',
                    'container_class' => '',
                    'container_id' => '',
                    'menu_class' => 'navbar-nav mr-auto',
                    'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
                    'walker' => new wp_bootstrap_navwalker())
            );
            ?>
        </div>
    </div>
</nav>
<div class="sidenav-overlay"></div>



