jQuery(document).ready(function ($) {
    $('.image-overlay .content').each(function () {
        var text = $(this).text();
        var words = text.split(" ");
        var sentence = "";

        words.forEach(function (value, index, array) {
            if (index === words.length - 1) {
                value = "<span class='last-word'>"+value+"</span>"
            }
            sentence += " " + value;
        });

        $(this).html(sentence);
    });
});