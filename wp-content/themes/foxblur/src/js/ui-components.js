jQuery(document).ready(function ($) {
    // ===== HERO IMAGE =======
    $('.hero-image-wrapper .hero-arrow').on('click', function () {
        $('html, body').animate({
            scrollTop: $(".striped-title.title-projecten").offset().top - 200
        }, 2000);
    });
    // ===== LIGHTBOX =======
    $('.single-projecten .content-wrapper img').each(function () {
        var $img = $(this),
            href = $img.attr('src');

        if ($img.attr('data-full-url')) {
            href = $img.attr('data-full-url');
        }
        $img.wrap('<a href="' + href + '" class="image-link slide-in"></a>');
    });

    if ($('.single-projecten').length) {
        var gallery = $('.image-link').simpleLightbox({
            doubleTapZoom: 2,
            maxZoom: 5,
            scaleImageToRatio: true,
            alertError: false,
        });

        var y = 0, x = 0;
        gallery.on('shown.simplelightbox', function () {
            $('.sl-image').first().prepend($('<span id="sl-image-hint">Dubbel klik zoom</span>'));
            $('#sl-image-hint').show();
            y = $('#sl-image-hint').offset().top - $(window).scrollTop();
            x = $('#sl-image-hint').offset().left;
        });


        $(document).on("mousemove", ".sl-image", function (event) {
            $("#sl-image-hint").css({
                top: event.clientY - (y - 10),
                left: event.clientX - (x - 10)
            });
        });

        gallery.on('close.simplelightbox', function () {
            $('#sl-image-hint').remove();
        });
    }
    setTimeout(function () {
        $('.image-overlay').on('mousemove', function () {
            if (!$(this).hasClass('active')) {
                $(this).addClass('active');
            }
        });
        $('.image-overlay').on('mouseout', function () {
            $(this).removeClass('active');
        });
    }, 3000);

    var loaded = false;

    // ===== SCROLLING =======

    function scrollUpAnimations(){
        var offsetUp = 200;
        $('.slide-in').each(function (i) {
            if ($(this).hasClass('skip-first') && i === 0 && !loaded) {
                $(this).addClass('show-slide-in');
                $(this).removeClass('show-slide-out');
                loaded = true
            } else {
                var objectTop = $(this).offset().top;
                var windowBottom = $(window).scrollTop() + $(window).innerHeight();

                    if (windowBottom - objectTop < offsetUp) {
                        if ($(this).hasClass('show-slide-in')) {
                            $(this).addClass('show-slide-out');
                            $(this).removeClass('show-slide-in');
                        }
                    }
            }
        });
    }

    function scrollDownAnimations() {
        var offset = 200;

        $('.slide-in').each(function (i) {
            if ($(this).hasClass('skip-first') && i === 0 && !loaded) {
                $(this).addClass('show-slide-in');
                $(this).removeClass('show-slide-out');
                loaded = true
            } else {
                var objectTop = $(this).offset().top;
                var windowBottom = $(window).scrollTop() + $(window).innerHeight();
                if (objectTop - offset < windowBottom) {
                    $(this).addClass('show-slide-in');
                    $(this).removeClass('show-slide-out');
                }
            }

        });
    }
    scrollDownAnimations();

    var lastScrollTop = 0, delta = 5;
    $(window).scroll(function () {
        var nowScrollTop = $(this).scrollTop();
        if(Math.abs(lastScrollTop - nowScrollTop) >= delta){
            if (nowScrollTop > lastScrollTop){
                scrollDownAnimations();
            } else {
                scrollUpAnimations();
            }
            lastScrollTop = nowScrollTop;
        }
    });
});