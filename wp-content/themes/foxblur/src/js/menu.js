jQuery(document).ready(function($){
    $('.navbar-toggler').click(function () {
        if ($('.navbar-toggler').hasClass('active')) {
            $(".sidenav-overlay").removeClass('active');
            $(".navbar-toggler").removeClass('active');
            $(".navbar-collapse").removeClass('active');
            $(".navbar-toggler").addClass('collapsed');
        }else{
            $(".navbar-toggler").addClass('active');
            $(".navbar-collapse").addClass('active');
            $(".sidenav-overlay").addClass('active');
            $(".navbar-toggler").removeClass('collapsed');
        }
    });
});