jQuery(document).ready(function($) {
    $('.owl-carousel-projecten').owlCarousel({
        loop:true,
        margin:0,
        nav:true,
        autoplay:true,
        autoplayTimeout:5000,
        smartSpeed: 2000,
       // lazyLoad: true,
        navText: [
            '<i class="fas fa-arrow-left"></i>',
            '<i class="fas fa-arrow-right"></i>'
        ],
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            1000:{
                items:3
            }
        }
    });

    $('.owl-carousel-recensies').owlCarousel({
        loop:true,
        margin:20,
        nav:false,
        autoplay:true,
        autoplayTimeout:5000,
        smartSpeed: 2000,
     //   lazyLoad: true,
        responsive:{
            0:{
                items:1
            },
            700:{
                items:2
            }
        }
    });
});