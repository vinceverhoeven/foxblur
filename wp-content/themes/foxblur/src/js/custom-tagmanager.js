jQuery(document).ready(function ($) {

    document.addEventListener('wpcf7mailsent', function (event) {
        gtag('event', 'mailing', {
            'send_to': 'UA-178917344-1'
        });
        console.log('mailing');
    }, false);


    $('a[href^="mailto:"]').on('click', function (e) {
        gtag('event', 'mailing', {
            'send_to': 'UA-178917344-1'
        });
        console.log('mailing');
    });
});
