var gulp = require('gulp'),
    sass = require('gulp-sass'),
    livereload = require('gulp-livereload');
    sourcemaps      = require('gulp-sourcemaps');

function style() {
    return gulp.src('src/scss/style.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error',sass.logError))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('.'))
        .pipe(livereload());
}
function watch() {
    livereload.listen(35729, function(err){
        if(err) return console.log(err);
    });
    gulp.watch('src/scss/**/*.scss',{ usePolling: true }, style)
}
exports.style = style;
exports.watch = watch;