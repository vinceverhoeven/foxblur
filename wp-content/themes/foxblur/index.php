<?php
get_header(); ?>
    <img class="full-image hero-image" src="<?php echo get_the_post_thumbnail_url(); ?>"/>
    <div class="cms-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12 default-content">
                    <h1 class="main-title">
                        <?php echo get_the_title(); ?>
                    </h1>
                    <div class="default-content-content">
                        <?php if (have_posts()) : while (have_posts()) : the_post();
                            echo get_the_content();
                        endwhile; ?>
                            <?php wp_reset_query(); ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="row call-to-action">
                <div class="col-md-12">
                    <div class="text-center">
                        <a class="btn-orange " href="/contact">Neem contact op</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer();
