<?php
/**
 * Template Name: homepage
 */
?>

<?php
get_header(); ?>
    <div class="home-page">
        <div class="page-wrapper">
            <section class="hero-image-wrapper">
                <img class="img-fluid" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="visualisatie 3d"/>
                <div class="hero-image-text-wrapper">
                    <div class="hero-image-text-block container-fluid">
                        <h1 class="title animate-fadein">
                            <?php echo get_the_title(); ?>
                        </h1>
                        <div class="subtitle animate-fadein">
                            <?php if (have_posts()) : while (have_posts()) : the_post();
                                echo get_the_content();
                            endwhile; ?>
                                <?php wp_reset_query(); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <span class="hero-arrow"></span>
            </section>
            <?php
            $projectInstance = new WebmangoProjectenOverview();
            $projecten = $projectInstance->getAllPosts();
            ?>
            <div class="container">
                <h2 class="striped-title title-projecten">
                    Laatste projecten
                </h2>
            </div>
            <div class="container-fluid nopadding">
                <div class="row">
                    <div class="item-slider owl-carousel owl-theme owl-carousel-projecten">
                        <?php
                        if ($projecten->have_posts()):
                            while ($projecten->have_posts()) : $projecten->the_post(); ?>
                                <div class="item">
                                    <a class="image-overlay" href="<?php the_permalink() ?>">
                                        <div class="image-overlay">
                                            <p class="content"><?php echo get_the_title(); ?></p>
                                        </div>
                                        <img class="slide-in" src="<?php echo $projectInstance->getThumbnailOrFeatured(get_the_ID()); ?>"
                                             alt="<?php echo get_the_title(); ?>"/>
                                    </a>
                                </div>
                            <?php endwhile;
                            wp_reset_query();
                        endif;
                        ?>
                    </div>
                </div>
            </div>
            <div class="container home-intro-tekst hide-overflow">
                <?php
                $snippetInstance = new WebmangoSnippetResourceModel();
                ?>
                <div class="row">
                    <div class="col-md-6 col-xs-12 col-left text-large-block slide-in">
                        <?php
                        $snippetLeftArray = $snippetInstance->getSnippet('home_intro_left');
                        echo $snippetInstance->getContent($snippetLeftArray);
                        ?>
                        <span class="striped-title-wrapper">
                     <a href="projecten">meer weten</a>
                </span>
                    </div>
                    <div class="offset-md-1 col-md-5 col-xs-12 col-right  text-small-block slide-in">
                        <?php
                        $snippetRightArray = $snippetInstance->getSnippet('home_intro_right');
                        echo $snippetInstance->getContent($snippetRightArray);
                        ?>
                    </div>
                </div>
            </div>
            <div class="container">
                <h2 class="striped-title title-team">
                    Ontmoet het team
                </h2>
            </div>
            <div class="container-fluid nopadding home-employee-block hide-overflow">
                <div class="container  slide-in">
                    <div class="row">
                        <?php $snippetHomeEmployee = $snippetInstance->getSnippet('home_employee'); ?>
                        <div class="col-md-7 col-xs-12 col-left nopadding-mob">
                            <img class="img-fluid"
                                 src="<?php echo $snippetInstance->getImage($snippetHomeEmployee); ?>"
                                 alt="ons team" title="ons team"/>
                        </div>
                        <div class="col-md-5 col-xs-12 col-right">
                            <p class="text-small-block"><?php echo $snippetInstance->getContent($snippetHomeEmployee); ?></p>
                            <div class="btn-wrapper">
                                <a href="contact" class="btn-orange">Neem contact op</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block-recensies container">
                <h2 class="striped-title title-recensies">
                    Klanten over ons
                </h2>
                <?php
                $recensiesInstance = new WebmangoRecensiesOverview();
                $recensies = $recensiesInstance->getAllPosts();
                ?>
                <div class="item-slider owl-carousel owl-theme owl-carousel-recensies slide-in">
                    <?php
                    if ($recensies->have_posts()):
                        while ($recensies->have_posts()) : $recensies->the_post(); ?>
                            <div class="item">
                                <div class="image-wrapper">
                                    <img  src="<?php echo get_the_post_thumbnail_url(); ?>"
                                         alt="<?php echo get_the_title(); ?>"/>
                                </div>
                                <span class="content">
                                <?php echo get_post_meta(get_the_ID(), 'recensie_content', true); ?>
                            </span>
                            </div>
                        <?php endwhile;
                        wp_reset_query();
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer();
