<?php
/**
 * Template Name: contactpage
 */
?>

<?php
get_header(); ?>
    <div class="contact-page">
        <div class="page-wrapper">
            <div class="container">
                <div class="row ">
                    <div class="offset-md-0 col-md-4 contact-sidebar">
                        <div class="sidebar-content">
                            <h1 class="main-title ">
                                <?php echo get_the_title(); ?>
                            </h1>
                            <p>Liever meteen antwoord?</p>
                            <a href="tel: 0102417469" class="btn-black">Bel</a>
                        </div>
                    </div>
                    <div class="col-md-6 contact-content">
                        <?php if (have_posts()) : while (have_posts()) : the_post();
                            echo get_the_content();
                        endwhile; ?>
                        <?php endif; ?>
                        <?php echo do_shortcode('[contact-form-7 id="170" title="Contact form 1"]'); ?>
                    </div>
                </div>
            </div>
            <div class="container-fluid nopadding">
                <div id="map"></div>
            </div>
        </div>
    </div>
<?php
get_footer();