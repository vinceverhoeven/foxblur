<?php
/**
 * Template Name: projecten-overview
 */
get_header();

$projectInstance = new WebmangoProjectenOverview();
$projecten = $projectInstance->getAllPosts();
$counter = 0;
?>
    <div class="page-projecten-overview">
        <div class="page-wrapper">
            <div class="container-fluid nopaddding">
                <div class="row">
                    <?php
                    if ($projecten->have_posts()):
                        while ($projecten->have_posts()) : $projecten->the_post(); ?>
                            <div class="<?php echo $projectInstance->getGrid($counter) ?> col-xs-12 grid-item nopadding">
                                <a class="link-projects-overview" href="<?php the_permalink() ?>">
                                    <div class="image-overlay">
                                        <p class="content"><?php echo get_the_title(); ?></p>
                                    </div>
                                    <img class="img-fluid slide-in skip-first" src="<?php echo $projectInstance->getThumbnailOrFeatured(get_the_ID()); ?>"
                                         alt="<?php echo get_the_title(); ?>">
                                </a>
                            </div>
                            <?php $counter++ ?>
                        <?php endwhile;
                        wp_reset_query();
                    endif;
                    ?>
                </>
            </div>
        </div>
    </div>
<?php
get_footer();
