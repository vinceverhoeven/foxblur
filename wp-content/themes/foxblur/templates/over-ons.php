<?php
/**
 * Template Name: over-ons
 */
get_header();

$werknemerInstance = new WebmangoMedewerkersOverview();
$werknemers = $werknemerInstance->getAllPosts();
?>
    <img class="full-image hero-image" src="<?php echo get_the_post_thumbnail_url(); ?>"/>
    <div class="page-wrapper page-over-ons">
        <div class="container">
                <h1 class="main-title">
                    <?php echo get_the_title(); ?>
                </h1>
                <?php if (have_posts()) : while (have_posts()) : the_post();
                    echo get_the_content();
                endwhile; ?>
                    <?php wp_reset_query(); ?>
                <?php endif; ?>
            </div>
            <div class="container medewerkers-block">
                <div class="row">
                    <?php
                    if ($werknemers->have_posts()):
                        while ($werknemers->have_posts()) : $werknemers->the_post(); ?>
                            <div class="col-xs-12 col-md-6 medewerkers-item-container">
                                <div class="medewerker-item">
                                    <div class="medewerker-item-image">
                                        <img class="img-fluid image-grey-scale"
                                             src="<?php echo get_the_post_thumbnail_url(); ?>"
                                             alt="<?php echo get_the_title(); ?>"/>
                                        <span class="medewerker-item-image-naam"><?php echo get_the_title(); ?></span>
                                        <span class="medewerker-item-image-functie"><?php echo get_post_meta(get_the_ID(), 'medewerker_functie', true); ?></span>
                                    </div>
                                    <div class="medewerker-item-content">
                                        <?php echo get_post_meta(get_the_ID(), 'medewerker_content', true); ?>
                                    </div>
                                    <div class="medewerker-item-extra">
                                        <div class="medewerker-item-extra-email">
                                            <?php $mail = get_post_meta(get_the_ID(), 'medewerker_email', true); ?>
                                            <a href="mailto: <?php echo $mail ?>"><?php echo $mail ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile;
                        wp_reset_query();
                    endif;
                    ?>
                </div>
                <div class="row call-to-action">
                    <div class="col-md-12">
                        <div class="text-center">
                            <a class="btn-orange " href="/foxblur/contact">Neem contact op</a>
                        </div>
                    </div>
                </div>
            </div>
    </div>
<?php
get_footer();
