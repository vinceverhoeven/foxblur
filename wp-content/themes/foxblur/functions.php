<?php
/**
 * webmango functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package webmango
 */
if (!function_exists('foxblur_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function foxblur_setup()
    {

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_filter('show_admin_bar', '__return_false');

        add_theme_support('title-tag');
        add_theme_support('custom-logo');
        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'menu-1' => esc_html__('Primary', 'foxblur'),
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));
    }
endif;
add_action('after_setup_theme', 'foxblur_setup');

function webmango_admin_cleanup()
{
    remove_menu_page('edit-comments.php');
}

add_action('admin_init', 'webmango_admin_cleanup');

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function webmango_widgets_init()
{
}

add_action('widgets_init', 'webmango_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function webmango_scripts()
{
    wp_enqueue_style('style.css', get_template_directory_uri() . '/style.css');
    wp_enqueue_script('bootstrap.js', get_template_directory_uri() . '/node_modules/bootstrap/dist/js/bootstrap.js', array('jquery'), '1.0.0', true);
    wp_enqueue_script('owl.carousel.js', get_template_directory_uri() . '/node_modules/owl.carousel/dist/owl.carousel.min.js', array('jquery'), '1.0.0', true);
    wp_enqueue_script('menu.js', get_template_directory_uri() . '/src/js/menu.js', array('jquery'), '1.0.0', true);
    wp_enqueue_script('slider.js', get_template_directory_uri() . '/src/js/slider.js', array('jquery'), '1.0.0', true);
    wp_enqueue_script('ui-components.js', get_template_directory_uri() . '/src/js/ui-components.js', array('jquery'), '1.0.0', true);
    wp_enqueue_script('grid.js', get_template_directory_uri() . '/src/js/grid.js', array('jquery'), '1.0.0', true);
    wp_enqueue_script( 'google-maps', '//maps.googleapis.com/maps/api/js?key=AIzaSyAcM2Ekv3_FwhFl9QAQSjjbmjbnFIqifcw', null, null, true );
    wp_enqueue_script('maps.js', get_template_directory_uri() . '/src/js/maps.js', array('jquery'), '1.0.0', true);
    wp_enqueue_script('custom-tagmanager.js', get_template_directory_uri() . '/src/js/custom-tagmanager.js', array('jquery'), '1.0.0', true);

    // lightbox
    wp_enqueue_script('lightbox.js', get_template_directory_uri() . '/node_modules/simplelightbox/dist/simple-lightbox.jquery.js', array('jquery'), '1.0.0', true);

    $dataToBePassed = array(
        'uri'            => get_stylesheet_directory_uri(),
    );
    wp_localize_script( 'maps.js', 'php_vars', $dataToBePassed );
}

add_action('wp_enqueue_scripts', 'webmango_scripts', 1);

// remove header links
function webmango_head_cleanup()
{
    // REMOVE WP EMOJI
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('wp_print_styles', 'print_emoji_styles');

    remove_action('template_redirect', 'rest_output_link_header', 11);
    remove_action('wp_head', 'wp_oembed_add_discovery_links');
    remove_action('wp_head', 'rest_output_link_wp_head');


    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('admin_print_styles', 'print_emoji_styles');

    remove_action('wp_head', 'rsd_link'); //removes EditURI/RSD (Really Simple Discovery) link.
    remove_action('wp_head', 'feed_links_extra', 3);                      // Category Feeds
    remove_action('wp_head', 'feed_links', 2);                            // Post and Comment Feeds
    remove_action('wp_head', 'rsd_link');                                 // EditURI link
    remove_action('wp_head', 'wlwmanifest_link');                         // Windows Live Writer
    remove_action('wp_head', 'index_rel_link');                           // index link
    remove_action('wp_head', 'wp_generator');
    remove_action('wp_head', 'wp_resource_hints', 2);
// WP version
}

add_action('init', 'webmango_head_cleanup');

require_once('lib/php/wp_bootstrap_navwalker.php');

function remove_core_updates(){

    global $wp_version;
    return (object) array(
        'last_checked' => time(),
        'version_checked' => $wp_version,
    );
}
add_filter('pre_site_transient_update_plugins', 'remove_core_updates');
add_filter('pre_site_transient_update_themes', 'remove_core_updates');

function your_theme_customizer_setting($wp_customize) {
// add a setting
    $wp_customize->add_setting('footer_logo');
// Add a control to upload the hover logo
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'footer_logo', array(
        'label' => 'Upload footer Logo',
        'section' => 'title_tagline', //this is the section where the custom-logo from WordPress is
        'settings' => 'footer_logo',
        'priority' => 8 // show it just below the custom-logo
    )));
}

add_action('customize_register', 'your_theme_customizer_setting');

function getCurrentUrl(){
    $url =  "//{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
    $escaped_url = htmlspecialchars( $url, ENT_QUOTES, 'UTF-8' );
    return $escaped_url;
}

add_filter('the_content', 'gs_add_img_lazy_markup', 15);  // hook into filter and use

function gs_add_img_lazy_markup($the_content) {

    libxml_use_internal_errors(true);
    $post = new DOMDocument();
    $post->loadHTML($the_content);
    $ps = $post->getElementsByTagName('p');
    // Iterate each img tag

    $classes = get_body_class();
    if (in_array('single-projecten',$classes)) {
        foreach( $ps as $p ) {
            $p->setAttribute('class', 'slide-in' );
        };
    }



    $imgs = $post->getElementsByTagName('img');

    // Iterate each img tag
    foreach( $imgs as $img ) {
        $img_id = preg_replace('/[^0-9]/', '', $img->getAttribute('class'));
        $image = wp_get_attachment_image_src($img_id,'full');

        if(isset($image[0])){
            $img->setAttribute('data-full-url', $image[0] );
        }
    };

    return $post->saveHTML();
}
