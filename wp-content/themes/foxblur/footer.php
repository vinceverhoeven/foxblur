<footer class="site-footer">
    <?php
    $snippetInstance = new WebmangoSnippetResourceModel();
    $snippetLeftFooter = $snippetInstance->getSnippet('footer_left');
    $snippetLeftFooterAddress = $snippetInstance->getSnippet('footer_address');

    ?>
    <div class="footer-content container">
        <div class="row">
            <div class="col-md-6 col-left">
                <?php echo $snippetInstance->getContent($snippetLeftFooter); ?>
                <div class="footer-socials">
                    <a href="https://www.facebook.com/foxblur.visuals/" target="_blank"><i class="fab fa-facebook-f"></i></a>
                    <a href="https://www.instagram.com/foxblur.visuals/" target="_blank"><i class="fab fa-instagram"></i></a>
                    <a href="https://www.linkedin.com/company/foxblur/" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                </div>
                <div class="footer-address">
                    <?php echo $snippetInstance->getContent($snippetLeftFooterAddress); ?>
                </div>
            </div>
            <div class="col-md-6 col-right">
                <img src="<?php echo get_theme_mod( 'footer_logo' ); ?>" alt="logo"/>
                <span>© <?php echo date("Y"); ?> foxblur.com</span>
                <?php
                wp_nav_menu(array(
                    'menu' => 'footer_menu_right'
                ));
                ?>
            </div>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>
</body>
</html>
